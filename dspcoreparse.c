/* --COPYRIGHT--,BSD
 * Copyright (c) 2010-2011, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/*
 * DSP coredump parsing utility.
 * This utility parses DSP coredump file genereted in ELF format.
 * This can be used to generete CCS lodable crashdump file from
 * from the ELF coredump file.
 * (http://processors.wiki.ti.com/index.php/Crash_Dump_Analysis)
 * This utility also can be used to get the coredump information.
 */


#include <stdio.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <getopt.h>
#include <limits.h>
#include "elf32.h"

#define VERSION "1.0.0.1"
#define SYSBIOS_VERSION "06320554"

#define VER_STRING VERSION":"SYSBIOS_VERSION

#ifndef EM_TI_C6000
#define EM_TI_C6000     140
#endif

#define perr(...) fprintf(stderr, __VA_ARGS__)
#define pinfo(...) fprintf(stdout, __VA_ARGS__)

#define check_limit(l) \
	do {if (--(l) < 0) return 0;} while(0)

/* From: http://processors.wiki.ti.com/index.php/Crash_Dump_Analysis */
#define CCS_CRASHDUMP_ID (521177)
#define CCS_CRASHDUMP_PROC_TYPE_C64XX (0x64)
#define CCS_CRASHDUMP_PROC_TYPE_C66XX (0x66)
#define CCS_CRASHDUMP_REG_ID "R"
#define CCS_CRASHDUMP_MEM_ID "M"
#define CCS_CRASHDUMP_MEM_PAGE_NONE (0)
#define CCS_CRASHDUMP_REG_FMT "0x0000000B"

/* Forward declaration */
char *reg_name_map[];

struct args {
	int verbose;
	long lines;
	char *infile;
	char *outfile;
};

static void usage(FILE * s)
{
	fprintf(s, "Usage: dspcoreparse [option(s)] <coredump file name>\n"
		"    --version	Print version information\n"
		" -h --help	Display this information\n"
		" -v --verbose	Print header information\n"
		" -l <lines>	Max number of lines in output file\n"
		" -o <file>	Place the ccs format coredump into <file>\n");
}

static int  parse_args(int argc, char ** argv, struct args *args)
{
	char c;
	struct option long_opt[] = {
		{"help", no_argument, 0, 'h'},
		{"verbose", no_argument, 0, 'v'},
		{"version", no_argument, 0, 's'},
		{0, 0, 0, 0}
	};
	int exit = 0;

	if (argc < 2) {
		usage(stderr);
		return 1;
	}

	args->lines = LONG_MAX; /* Unlimited */

	while ((c = getopt_long(argc, argv, ":hvsl:o:", long_opt, 0)) != EOF) {

		switch(c) {
		case 0:
			break;
		case 'v':
			args->verbose = 1;
			break;
		case 'o':
			args->outfile = optarg;
			break;
		case 'l':
			args->lines = atol(optarg);
			break;
		case ':':
			perr("Option -%c requires a parameter\n", optopt);
			usage(stderr);
			return 1;
		case 'h':
			usage(stdout);
			return 1;
		case 's':
			pinfo("DSP coredump parsing utility version: %s\n",
			      VER_STRING);
			return 1;
		case '?':
		default:
			perr("Invalid option '-%c'\n", optopt);
			usage(stderr);
			return 1;
		}
	}

	if (argc <= optind) {
		perr("Input coredump filename not present\n");
		usage(stderr);
		return 1;
	}
	args->infile = argv[optind];

	return 0;
}

int parse_elf32_header(FILE *fi, struct Elf32_Ehdr *eh)
{
	int n;

	n = fread(eh, 1, sizeof(struct Elf32_Ehdr), fi);
	if (n != sizeof(*eh)) {
		perr("error in reading ELF header (%d)\n", n);
		return -1;
	}

	if ((eh->e_ident[EI_MAG0] != ELFMAG0) ||
		(eh->e_ident[EI_MAG1] != ELFMAG1) ||
		(eh->e_ident[EI_MAG2] != ELFMAG2) ||
		(eh->e_ident[EI_MAG3] != ELFMAG3)) {
		perr("Invalid ELF file, incorrect elf magic\n");
		return -1;
	}

	/* Support only 32 bit format */
	if (eh->e_ident[EI_CLASS] != ELFCLASS32) {
		perr("Invalid ELF file, incorrect ident:EI_CLASS (%d)\n",
		     eh->e_ident[EI_CLASS]);
		return -1;
	}

	/* Support only 2's complement, little endian */
	if (eh->e_ident[EI_DATA] != ELFDATA2LSB) {
		perr("Invalid ELF file, incorrect ident:EI_DATA (%d)\n",
		     eh->e_ident[EI_DATA]);
		return -1;
	}

	/* Currently limit to coredump file only */
	if (eh->e_type != ET_CORE) {
		perr("Invalid ELF file, incorrect e_type (%d)\n", eh->e_type);
		return -1;
	}

	/* Only support C6000 family */
	if (eh->e_machine != EM_TI_C6000) {
		perr("Invalid ELF file, incorrect e_machine (%d)\n",
		     eh->e_machine);
		return -1;
	}

	/* Now check for valid program section informations */
	if (!eh->e_phoff || !eh->e_phnum || !eh->e_phentsize) {
		perr("Invalid ELF file, invalid e_phoff|e_phnum|e_phentsize\n");
		return -1;
	}

	return 0;
}

void print_elf32_header(struct Elf32_Ehdr *eh)
{
	pinfo("\nELF Header:\n");
	pinfo("Identifier\t0x%02x %c%c%c\n",
	      eh->e_ident[EI_MAG0], eh->e_ident[EI_MAG1],
	      eh->e_ident[EI_MAG2], eh->e_ident[EI_MAG3]);
	pinfo("Class\t\t%d\n", eh->e_ident[EI_CLASS]);
	pinfo("Data\t\t%d\n", eh->e_ident[EI_DATA]);
	pinfo("Version\t\t%d\n", eh->e_ident[EI_VERSION]);
	pinfo("Type\t\t%d\n", eh->e_type);
	pinfo("Machine\t\t%d\n", eh->e_machine);
	pinfo("phoff\t\t%d\n", eh->e_phoff);
	pinfo("phnum\t\t%d\n", eh->e_phnum);
	pinfo("phentsize\t%d\n", eh->e_phentsize);
}

int parse_phdr(FILE *fi, struct Elf32_Ehdr *eh, struct Elf32_Phdr *ph)
{
	int i;
	int n;

	for (i = 0; i < eh->e_phnum; i++) {
		fseek(fi, eh->e_phoff + (i * eh->e_phentsize), SEEK_SET);
		n = fread(&ph[i], 1, sizeof(struct Elf32_Phdr), fi);
		if (n < sizeof(struct Elf32_Phdr)) {
			perr("Invalid phdr[%d] read (%d)\n", i, n);
			return -1;
		}
	}
	return 0;
}

void print_phdr(struct Elf32_Ehdr *eh, struct Elf32_Phdr *ph)
{
	int i;

	pinfo("\nProgram Header:\n");

	pinfo("type\toffset\t\tpaddr\t\tfilesz\t\tmemsz\t\tflags\talign\n");

	for (i = 0; i < eh->e_phnum; i++) {
		pinfo("%d\t0x%08x\t0x%08x\t0x%08x\t0x%08x\t0x%x\t0x%x\n",
		      ph[i].p_type, ph[i].p_offset, ph[i].p_paddr,
		      ph[i].p_filesz, ph[i].p_memsz, ph[i].p_flags,
		      ph[i].p_align);
	}
}

int write_notes_section(FILE *fi, FILE *fo, long *l, struct Elf32_Phdr *ph)
{
	struct Elf32_Nhdr nh;
	int reg_offset;
	uint32_t reg_val;
	int offset;
	int i;
	int n;

	fseek(fi, ph->p_offset, SEEK_SET);
	n = fread(&nh, 1, sizeof(struct Elf32_Nhdr), fi);
	if (n < sizeof(struct Elf32_Nhdr)) {
		perr("Can not read notes header (%d)\n", n);
		return -1;
	}

	if (!fo) {
		char *np;
		pinfo("namesz\t0x%x\n", nh.n_namesz);
		pinfo("descsz\t0x%x\n", nh.n_descsz);
		pinfo("type\t0x%x\n", nh.n_type);
		np = calloc(nh.n_namesz + 1, 1);
		n = fread(np, 1, nh.n_namesz, fi);
		if (n < nh.n_namesz) {
			perr("Can not read notes name (%d)\n", n);
			return -1;
		}
		pinfo("name\t%s\n", np);
		free (np);
	}

	offset = ph->p_offset + sizeof(struct Elf32_Nhdr);
	offset += ((nh.n_namesz + 3) & ~0x03);
	/* Jump to descriptor field */
	fseek(fi, offset, SEEK_SET);

	for (i = 0; i < (nh.n_descsz / 4); i++){
		if (reg_name_map[i] == 0)
			return 0;

		n = fread(&reg_val, 1, sizeof(uint32_t), fi);
		if (n < sizeof(uint32_t)) {
			perr("Error in reading notes register (%d)\n", n);
			return -1;
		}

		if (fo) {
			check_limit(*l);
			fprintf(fo, "%s %s %s 0x%08X\n",
				CCS_CRASHDUMP_REG_ID, reg_name_map[i],
				CCS_CRASHDUMP_REG_FMT, reg_val);
			continue;
		}
		pinfo("%s\t0x%08X", reg_name_map[i], reg_val);
		if ((i + 1) % 3 == 0)
			pinfo("\n");
		else
			pinfo("\t\t");
	}

	return 0;
}

void print_notes(FILE *fi, struct Elf32_Ehdr *eh, struct Elf32_Phdr *ph)
{
	int i;

	pinfo("\nNotes Section:\n");

	/* Find notes section */
	for (i = 0; i < eh->e_phnum; i++) {
		if (ph[i].p_type != PT_NOTE)
			continue;
		write_notes_section(fi, 0, 0, &ph[i]);
		return; /* Only support one note section */
	}
}

int write_program_section(FILE *fi, FILE *fo, long *l, struct Elf32_Phdr *ph)
{
	int i;
	int c;
	fseek(fi, ph->p_offset, SEEK_SET);
	check_limit(*l);
	fprintf(fo, "%s 0x%08x 0x%08x 0x%x\n",
		CCS_CRASHDUMP_MEM_ID, CCS_CRASHDUMP_MEM_PAGE_NONE,
		ph->p_paddr, ph->p_memsz);

	for (i = 0; i < ph->p_filesz; i++) {
		c = fgetc(fi);
		if (c == EOF) {
			perr("Unexpected EOF reading program section\n");
			return -1;
		}
		check_limit(*l);
		fprintf(fo, "0x%02x\n", c);
	}

	if (ph->p_memsz <= ph->p_filesz)
		return 0;

	/* Fill zero for the rest of the area */
	c = 0;
	for (i = 0; i < (ph->p_memsz - ph->p_filesz); i++) {
		check_limit(*l);
		fprintf(fo, "0x%02x\n", c);
	}
	return 0;

}

void write_ccs_coredump(FILE *fi, FILE *fo, long l, struct Elf32_Ehdr *eh,
						struct Elf32_Phdr *ph)
{
	int i;

	/* Write CCS coredump header */
	if (--l < 0) return;
	fprintf(fo, "%d %d\n", CCS_CRASHDUMP_ID, CCS_CRASHDUMP_PROC_TYPE_C66XX);

	/* First write notes section */
	for (i = 0; i < eh->e_phnum; i++) {
		if (ph[i].p_type != PT_NOTE)
			continue;
		if (write_notes_section(fi, fo, &l, &ph[i]))
			return;
		if (l < 1) return;
	}

	/* Write other program sections */
	for (i = 0; i < eh->e_phnum; i++) {
		if (ph[i].p_type == PT_NOTE)
			continue;
		if (write_program_section(fi, fo, &l, &ph[i]))
			return;
		if (l < 1) return;
	}
}

int main(int argc, char *argv[])
{
	struct args args = {0};
	FILE *fi = 0;
	FILE *fo = 0;
	struct Elf32_Ehdr eh;
	struct Elf32_Phdr *ph = 0;

	if (parse_args(argc, argv, &args) != 0)
		goto done;

	fi = fopen(args.infile, "rb");
	if (!fi) {
		perr("parsecoredump: can't open file %s (errno:%d)\n",
		       args.infile, errno);
		goto done;
	}

	if (parse_elf32_header(fi, &eh))
		goto done;

	if (args.verbose) {
		pinfo("DSP coredump parsing utility (version %s)\n",
		      VER_STRING);
		print_elf32_header(&eh);
	}

	ph = calloc(sizeof(struct Elf32_Phdr), eh.e_phnum);
	if (!ph) {
		perr("parsecoredump: can't allocate phdr array (errno:%d)\n",
		       errno);
		goto done;
	}

	if (parse_phdr(fi, &eh, ph))
		goto done;

	if (args.verbose) {
		print_phdr(&eh, ph);
		print_notes(fi, &eh, ph);
		pinfo("\n");
	}

	if (!args.outfile)
		goto done;

	fo = fopen(args.outfile, "w");
	if (!fo) {
		perr("parsecoredump: can't open out file %s (errno:%d)\n",
		       args.outfile, errno);
		goto done;
	}

	write_ccs_coredump(fi, fo, args.lines, &eh, ph);

done:
	if (fi)
		fclose(fi);
	if (fo)
		fclose(fo);
	if (ph)
		free(ph);
	return 0;
}

/* Register map from
 * bios_6_32_05_54\packages\ti\sysbios\family\c64p\Exception.h
 *
 * The order of elements should exactly match
 * ti_sysbios_family_c64p_Exception_Status,
 * ti_sysbios_family_c64p_Exception_Context and
 * structures from Exception.h file.
 * */

char *reg_name_map[] = {

	"PC",
	"EFR", /* start:ti_sysbios_family_c64p_Exception_Status */
	"NRP",
	"NTSR",
	"IERR", /* end:ti_sysbios_family_c64p_Exception_Status */

	"ILC", /* start:ti_sysbios_family_c64p_Exception_Context */
	"RILC",
	"AMR",
	"SSR",
	"IRP",
	"NRP",
	"ITSR",
	"NTSR",
	"EFR",
	"IERR",
	"B30",
	"B31",
	"B28",
	"B29",
	"B26",
	"B27",
	"B24",
	"B25",
	"B22",
	"B23",
	"B20",
	"B21",
	"B18",
	"B19",
	"B16",
	"B17",
	"B14",
	"B15",
	"B12",
	"B13",
	"B10",
	"B11",
	"B8",
	"B9",
	"B6",
	"B7",
	"B4",
	"B5",
	"B2",
	"B3",
	"B0",
	"B1",
	"A30",
	"A31",
	"A28",
	"A29",
	"A26",
	"A27",
	"A24",
	"A25",
	"A22",
	"A23",
	"A20",
	"A21",
	"A18",
	"A19",
	"A16",
	"A17",
	"A14",
	"A15",
	"A12",
	"A13",
	"A10",
	"A11",
	"A8",
	"A9",
	"A6",
	"A7",
	"A4",
	"A5",
	"A2",
	"A3",
	"A0",
	"A1", /* end:ti_sysbios_family_c64p_Exception_Context */

	0 /* end marker */
};
